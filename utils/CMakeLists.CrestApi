
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

# Set the name of the package.
project( CrestApiLib CXX )

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

SET(CMAKE_INCLUDE_PATH ${CMAKE_INCLUDE_PATH} $ENV{BOOST_LIB_DIR_PATH})
SET(CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH} $ENV{BOOST_LIB_DIR_PATH}/lib ../build/CrestApi)


# External dependencies.
find_package( CURL )
find_package( Boost )

SET(Boost_INCLUDE_DIRS $ENV{BOOST_LIB_DIR_PATH}/include)

include_directories(CrestApi ./ nlohmann ${Boost_INCLUDE_DIRS})

set (HEADERS CrestApi/CrestApi.h CrestApi/CrestApiExt.h nlohmann/json.hpp)
set (SOURCES src/CrestApi.cxx)
set (SOURCES2 doc/crest_example.cxx)

if(${CMAKE_INSTALL_PREFIX} STREQUAL "/usr/local") 
    set(CMAKE_INSTALL_PREFIX ${CMAKE_CURRENT_SOURCE_DIR}/../installed)
endif()

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++17" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 ")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x ")
else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

# Component(s) in the package.
add_library (CrestApiLib SHARED ${HEADERS} ${SOURCES}
   ${CURL_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
   ${CURL_LIBRARIES} ${Boost_LIBRARIES} )
target_link_libraries(CrestApiLib stdc++fs)


add_executable(crest_example ${SOURCES2} ${HEADERS}
   ${CURL_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
   ${CURL_LIBRARIES} ${Boost_LIBRARIES} 
)
target_link_libraries(crest_example stdc++fs CrestApiLib curl)

install(TARGETS crest_example DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/)
install(TARGETS CrestApiLib DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/)


file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/CrestApi/CrestApi.h ${CMAKE_CURRENT_SOURCE_DIR}/CrestApi/CrestApiExt.h 
     DESTINATION ${CMAKE_INSTALL_PREFIX}/include/CrestApi)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/nlohmann/json.hpp 
     DESTINATION ${CMAKE_INSTALL_PREFIX}/include/nlohmann/)
