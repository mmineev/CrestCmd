CREST Command Line Client (CrestCmd)

Short Instruction.

Here is a description how to compile and setup crestCmd.


1) Clone CrestApi package:

> git clone https://gitlab.cern.ch/mmineev/CrestApi.git

2) Clone CrestCmd package:

> git clone https://gitlab.cern.ch/mmineev/CrestCmd.git

3) Create a "build" directory:

> mkdir ./build

4) Copy a common CMakeLists.txt:

> cp ./CrestApi/CMakeLists.txt-parent ./CMakeLists.txt

5) Setup:

> source ./CrestApi/setup.sh

6) Compilation:

> cd ./build
> cmake -Wno-dev ..
> make

7) Setup for executables:

> source ./x86_64-centos7-gcc8-opt/setup.sh

8) To set a CREST_SERVER_PATH variable (CREST Server URL) use the command:

> export CREST_SERVER_PATH=http://crest-01.cern.ch:8090

(or run the script:
> source ../CrestCmd/scripts/crest-setup.sh)

9) CREST Command Line Examples:

a. Example, how to get a tag with the name "test_MvG3":

> crestCmd get tag -n test_MvG3

b. Example, how to get a crestCmd command list:

> crestCmd get commands

---------------

The full User's Guide is here:

/CrestCmd/doc/crestCmd-User-Guide.txt

It contains the description of the crestImport and crestExport utilities too.



